import 'package:flutter/material.dart';
import 'package:flutters4/form.dart';
import 'package:flutters4/list.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => HomePageState();
}

class HomePageState extends State<HomePage> with TickerProviderStateMixin{
  List<Map<String, dynamic>> _list = [];
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = new TabController(vsync: this, length: 2);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }


  _add(Map<String, dynamic> record) {
    FocusScope.of(context).requestFocus(new FocusNode());
    _list.add(record);
    _tabController.animateTo(1);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
            appBar: AppBar(title: Text('List'),),
            body: TabBarView(
              controller: _tabController,
              children: [
                FormPage(add: _add),
                ListPage(list: _list)
              ]
            ),
            bottomNavigationBar: Material(
              color: Colors.blue,
              child: TabBar(
                controller: _tabController,
                tabs: <Widget>[
                  Tab(
                    icon: Icon(Icons.create),
                  ),
                  Tab(
                    icon: Icon(Icons.list),
                  )
                ],
              ),
            ),
          );
  }
}