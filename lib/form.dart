import 'package:flutter/material.dart';

class FormPage extends StatefulWidget {
  final Function add;

  FormPage({ this.add });

  @override
  State<StatefulWidget> createState() => FormPageState();
}

class FormPageState extends State<FormPage> {

  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  String _titleValue;
  String _descriptionValue;
  double _priceValue;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: ListView(
        padding: EdgeInsets.all(20.0),
        children: <Widget>[
          TextFormField(
            decoration: InputDecoration(labelText: 'Titulo'),
            onSaved: (String value) {
              setState(() {
                _titleValue = value;
              });
            },
          ),
          TextFormField(
            decoration: InputDecoration(labelText: 'Descripción'),
            onSaved: (String value) {
              setState(() {
                _descriptionValue = value;
              });
            },
          ),
          TextFormField(
            keyboardType: TextInputType.number,
            decoration: InputDecoration(labelText: 'Precio'),
            onSaved: (String value) {
              setState(() {
                _priceValue = double.parse(value);
              });
            },
          ),
          RaisedButton(
            color: Colors.blue,
            textColor: Colors.white,
            onPressed: () {
              this._formKey.currentState.save();
              Map<String, dynamic> record = {
                "title": _titleValue,
                "description": _descriptionValue,
                "price": _priceValue,
              };
              this.widget.add(record);
            },
            child: Text('Save'),
          ),
          SizedBox(height: 20.0,),
          Text('Titulo: $_titleValue'),
          Text('Descripción: $_descriptionValue'),
          Text('Precio: $_priceValue'),
        ],
      ),
    );
  }
}