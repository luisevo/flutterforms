import 'package:flutter/material.dart';

class ListPage extends StatelessWidget {
  final List<Map<String, dynamic>> list;
  ListPage({this.list});

  @override
  Widget build(BuildContext context) {
    return ListView(
        children: this.list.map((Map<String, dynamic> item){
          return ListTile(
            title: Text(item["title"]),
            subtitle: Text(item["description"])
          );
        }).toList(),
      );
  }
}